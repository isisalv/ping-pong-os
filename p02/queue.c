//a1799169  - Victor Hugo Gabriel - 2019/2

#include <stdio.h>
#include "queue.h"
#include <stdbool.h>
//------------------------------------------------------------------------------
// Insere um elemento no final da fila.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - o elemento deve existir
// - o elemento nao deve estar em outra fila

void queue_append (queue_t **queue, queue_t *elem){
    if(elem == NULL || (elem->next != NULL && elem->prev!=NULL) || queue == NULL){
        return;
    }else{

        if((*queue) == NULL){
            elem->next = elem;
            elem->prev = elem;
            (*queue) = elem;
            return;

        }else{

            elem->prev = (*queue)->prev;
            elem->next = (*queue);
            (*queue)->prev->next = elem;
            (*queue)->prev = elem;

        }

    }
}

int queueBelongs(queue_t *queue, queue_t *elem){
    int belongs = 0;
    queue_t *aux = queue;

    if(queue == elem){
        return 1;
    }

    while(aux->next != queue ){
        aux = aux->next;
        if(aux == elem){
            belongs = 1;
            return belongs;
        }
    }

    return belongs;
}


//------------------------------------------------------------------------------
// Remove o elemento indicado da fila, sem o destruir.
// Condicoes a verificar, gerando msgs de erro:
// - a fila deve existir
// - a fila nao deve estar vazia
// - o elemento deve existir
// - o elemento deve pertencer a fila indicada
// Retorno: apontador para o elemento removido, ou NULL se erro

queue_t *queue_remove (queue_t **queue, queue_t *elem){
    if( ((*queue) == NULL) || ((*queue)->next == NULL || (*queue)->prev == NULL) || elem == NULL || queueBelongs(*queue, elem) == 0 ){
        return;
    }else{
        queue_t *aux_next, *aux_prev,*aux_delete;

        if(elem == (*queue)){
            aux_delete = (*queue);
            if(aux_delete->next == (*queue) && aux_delete->prev == (*queue)){
                (*queue) = NULL;
                aux_delete->next = NULL;
                aux_delete->prev = NULL;
                return aux_delete;
            }else{
                aux_next = (*queue)->next;
                aux_next->prev = (*queue)->prev;
                aux_prev = aux_next->prev;
                aux_prev->next = aux_next;
                (*queue) = aux_next;
            }
        }
        else if(elem == (*queue)->prev){
            aux_delete = (*queue)->prev;
            (*queue)->prev->prev->next = (*queue);
            (*queue)->prev = (*queue)->prev->prev;
        }else{
            while((*queue)->next != elem){
                (*queue) = (*queue)->next;
            }
            aux_delete = (*queue)->next;
            (*queue)->next->next->prev = (*queue);
            (*queue)->next = (*queue)->next->next;
        }

        aux_delete->next = NULL;
        aux_delete->prev = NULL;
        return aux_delete;
    }
}

//------------------------------------------------------------------------------
// Conta o numero de elementos na fila
// Retorno: numero de elementos na fila

int queue_size (queue_t *queue){
    int tamanho = 0;

    if(queue != NULL){
        tamanho = 1;
        queue_t *aux = queue;
        while(aux->next != queue){
            aux = aux->next;
            tamanho++;
        }
    }
    return tamanho;

}

//------------------------------------------------------------------------------
// Percorre a fila e imprime na tela seu conteúdo. A impressão de cada
// elemento é feita por uma função externa, definida pelo programa que
// usa a biblioteca. Essa função deve ter o seguinte protótipo:
//
// void print_elem (void *ptr) ; // ptr aponta para o elemento a imprimir

void queue_print (char *name, queue_t *queue, void print_elem (void*) ){
    printf(name);

    if(queue == NULL){
        print_elem(NULL);
    }else{
        queue_t* aux = queue;
        do{
            print_elem(aux);
            aux = aux->next;
            printf(" ");
        }while(aux != queue);
    printf("\n");
    }
}

