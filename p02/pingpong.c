#include "datatypes.h"
#include <stdio.h>
#include <ucontext.h>

#define STACKSIZE 32768		/* tamanho de pilha das threads */

char *stack;
int id;
task_t taskMain, *currentTask, *previousTask;
ucontext_t currentContext, mainContext;

void pingpong_init ()
{
    setvbuf(stdout, 0, _IONBF, 0);
    id = 0;
    taskMain.prev = NULL;
    taskMain.next = NULL;
    taskMain.tid = id;
    getcontext(&(taskMain.context));
    currentTask = &taskMain;

}

int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg)
{
    ucontext_t currentContext = task->context;
    task->next = NULL;
    task->prev = NULL;
    getcontext (&currentContext);

   stack = malloc (STACKSIZE) ;

   if (stack)
   {
      currentContext.uc_stack.ss_sp = stack ;
      currentContext.uc_stack.ss_size = STACKSIZE;
      currentContext.uc_stack.ss_flags = 0;
      currentContext.uc_link = NULL;
   }
   else
   {
      perror ("Erro na criação da pilha: ");
      exit (1);
   }

   makecontext (&currentContext, (void(*)(void))start_func, 1, arg);

   task->context = currentContext;
   id++;
   task->tid = id;

   printf("\nTask [%d] Criada!", id);
}

void task_exit (int exitCode)
{
    task_switch(&taskMain);
}

int task_switch (task_t *task)
{
    previousTask = currentTask;
    currentTask = task;
    if (swapcontext(&(previousTask->context), &(currentTask->context)) == -1){
        printf("swap error");
        return -1;
    }

    return 0;
}

int task_id ()
{
    return (*currentTask).tid;
}
