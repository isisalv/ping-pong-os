#include "datatypes.h"
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "queue.h"

#define STACKSIZE 32768		/* tamanho de pilha das threads */

char *stack;
int id, userTasks=-1;
task_t taskMain, *currentTask, *previousTask, dispatcher;
ucontext_t currentContext, mainContext;
queue_t *ready_queue;

void dispatcher_body (void * arg) // dispatcher é uma tarefa
{
    while ( userTasks > 0 )
    {
        printf("\nuserTasks -> [%d]", userTasks);
        userTasks--;
        /*
        next = scheduler() ; // scheduler é uma função
        if (next)
        {
            ... // ações antes de lançar a tarefa "next", se houverem
            task_switch (next) ; // transfere controle para a tarefa "next"
            ... // ações após retornar da tarefa "next", se houverem
        }
        */
    }
    task_exit(0) ; // encerra a tarefa dispatcher
}

void pingpong_init ()
{
    setvbuf(stdout, 0, _IONBF, 0);
    id = 0;
    taskMain.prev = NULL;
    taskMain.next = NULL;
    taskMain.tid = id;
    getcontext(&(taskMain.context));
    currentTask = &taskMain;
    task_create (&dispatcher, dispatcher_body, "   Dispatcher") ;
}

int task_create (task_t *task,			// descritor da nova tarefa
                 void (*start_func)(void *),	// funcao corpo da tarefa
                 void *arg)
{
    ucontext_t currentContext = task->context;
    task->next = NULL;
    task->prev = NULL;
    getcontext (&currentContext);

   stack = malloc (STACKSIZE) ;

   if (stack)
   {
      currentContext.uc_stack.ss_sp = stack ;
      currentContext.uc_stack.ss_size = STACKSIZE;
      currentContext.uc_stack.ss_flags = 0;
      currentContext.uc_link = NULL;
   }
   else
   {
      perror ("Erro na criação da pilha: ");
      exit (1);
   }

   makecontext (&currentContext, (void(*)(void))start_func, 1, arg);

   task->context = currentContext;
   if(id > 0) queue_append ((queue_t **) &ready_queue, (task_t*) &task) ;
   id++;
   userTasks++;
   task->tid = id;
   printf("\n%s Criada!", (char *) arg);
}

int task_switch (task_t *task)
{
    previousTask = currentTask;
    currentTask = task;
    if (swapcontext(&(previousTask->context), &(currentTask->context)) == -1){
        printf("swap error");
        return -1;
    }

    return 0;
}

void task_exit (int exitCode)
{
    task_switch(&taskMain);
}

int task_id ()
{
    return (*currentTask).tid;
}


void task_yield ()
{
    int currentTaskId = task_id();
    if(currentTaskId != taskMain.tid){
        printf("\nManda tarefa atual p o final da fila de prontas");
    }

    task_switch(&dispatcher);
}

